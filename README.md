Spreadsheet for planning and tracking deliverable submissions
===================

Set up for engineering drawings, the spreadsheet allows you to plan the sheet information and team responsibilities.  This project arises from a need for peers across firms to more effectively plan together.  

The person organizing the work effort can use spreadsheet to estimate overall effort, plan what sheets comprise each deliverable, and remember which team members tackle which sheets.  

![format view](img-20220602-i1-3-view_format.jpg)

## Table of Contents

* [Usage](#usage) 
* [Installation](#installation) 
* [Uses](#uses) 
* [Needs](#needs) 
* [Contributing](#contributing) 
* [Credits](#credits) 
* [License](#license) 
* [Donations](#donations)


## Usage

This project involves using a Microsoft Excel spreadsheet to consolidate sheet information and create a summary list suitable for cutting and pasting into a CADD file.  The team can also use the sheet to share progress assessments for each sheet.  

Rough guide included: 
`inf-scdesigns-drawing_list_procedure-R001.docx`

Spreadsheet tool:
`reg-scdesigns-drawing_list-TEMPLATE-20160427.xlsm`

Two tabs in the file separate the "drawing dashboard" interface and the output for CADD use.  

Sheet 1:
 
1. Enter the project information, and 4 lines for the sheet titles

2. Enter the deliverable names and dates

3. Enter the team member's names for each sheet

4. Enter the qualitative progress on each sheet

5. Enter the status for each sheet: include or exclude from current deliverable

Sheet 2:

1. Click the "button" at the top to update the drawing list based on which sheets are tagged for inclusion in current deliverable

2. Copy the cells to the clipboard and paste into the CADD file


## Installation 

1. Download the repository to where you intend to work  
2. Extract the project into the folder  


## Needs

The second sheet needs to use the macro functions to create the sheet list that only chooses the sheets tagged for the current deliverable.  

## Requirements

 * Microsoft Excel and Microsoft Word (still need to test with other programs, likely to work with LibreOffice)

<!--
#### Debian
```bash
sudo apt install python3-feedparser
```

#### Fedora
```bash
sudo dnf install pandoc texlive-collection-context
```

#### Arch
```bash
sudo pacman -S pandoc texlive-core
```
-->
## Troubleshooting


## Contributing

Please do.  

## Credits

* Scott Cameron, ASTTBC

## License

Creative Commons Share and Share Alike, latest.

## Donations

TODO
<!--
We accept donations at the [VECTOR website](https://vectorradio.ca/donate/)
-->
